# CHANGLOG

## PreProcess Class
- String GetLocalPath() -> 
    - get directory of "/dataset"
- String ExtractContent() 
    - return String content
- String ExtractContent(File)
    - return String content from (File)
- Void Listing() 
    - List all file in "/dataset" 

## StopWord Class - DEPRECATED
- ArrayList<String> WordSpiltter(String content) - LEGACY
- boolean IsStopWord(String word, String stopword) - LEGACY
- ArrayList<String> TermCrawler(ArrayList<String> terms)
- String WordsMerger(String[] words) - LEGACY

## WordList Class
- Crawling

## WordProccesing Class - Static
- String Normalized(String) -> to lowercase
- ArrayList<String> Splitter(String) -> split string into list of word
- String Merger (Arraylist<String>) -> merge list of words into single string
- ArrayList<String> (ArrayList<String>, String[]) -> crawling
- boolean IsQualified (String target, String source) -> compare target with source, keep matched word

