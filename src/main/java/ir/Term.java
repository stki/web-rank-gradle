package ir;
import java.util.ArrayList;

public class Term {

    class Prop
    {
        boolean[] docFrequency = new boolean[10];
        int[] termFrequency = new int[10];
        double inverseDocFreq;
    }

    private String word;
    public Prop prop = new Prop();

    public Term()
    {

    }

    public Term(String word)
    {
        this.word = word;
    }

    public String GetWord()
    {
        return word;
    }

    public int GetDocFrequency()
    {
        int counter = 0;
        for (boolean docfreq : prop.docFrequency) 
        {
            if(docfreq)
                counter++;
        }
        return counter;
    }


    public void SetInverseDocFreq(int totalDoc)
    {
        this.prop.inverseDocFreq = Math.log10((double) totalDoc/GetDocFrequency());
    }
}