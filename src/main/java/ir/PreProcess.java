package ir;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.jsoup.*;
import org.jsoup.nodes.Document;

public class PreProcess {

    String title = "5 GitHub tips for new coders.html"; 
    private WordList wordlist;

    public ArrayList<String> queryTerms;
    public ArrayList<String> qualifiedTerms;

    public ArrayList<Integer> termFrequency;

    public PreProcess(WordList wordlist, String query){
        this.wordlist = wordlist;
        queryTerms = wordlist.Crawling(query);
        qualifiedTerms = wordlist.Crawling(ContentExtraction());
        termFrequency = WordProcessing.TermFrequency(queryTerms, qualifiedTerms);
    }

    public PreProcess(WordList wordList)
    { 
        this.wordlist = wordList;
    }

    // Test -> title is defined
    public String ContentExtraction()
    {
        File file = new File(GetLocalPath() + title);
        try {
            Document document = Jsoup.parse(file, "UTF-8");
            return document.text();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String ContentExtraction(File file)
    {
        try {
            Document document = Jsoup.parse(file, "UTF-8");
            return document.text();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // remove any non-revelant term within arraylist
    public void ContentCleaning()
    {

    }












    // -------------------------------------------------------

    public String GetLocalPath(){

        String localPath = "/web-rank/files/dataset/";
        File directory = new File(System.getProperty("user.dir"));
        directory = directory.getParentFile();
        return directory.getAbsolutePath() + localPath;
    }

    public void Listing(){

        String sourceDirectory = GetLocalPath();
        File directory = new File(sourceDirectory);

        if(directory.isDirectory()){

            File[] fileList = directory.listFiles();
            for (File file : fileList) {
                System.out.println(file.getName());
            }
        }
    }
    

    //mengambil filedr folder
    //dirpath (lokasi folder file HTML)
    public void getFileFolder(File dirPath){
        
        BufferedReader bufferedReader = null;
        FileReader fileReader = null;
        File files[] = null;
        
        if(dirPath.isDirectory()){
            files = dirPath.listFiles();
            for(File file:files){
                if(file.isDirectory())
                    getFileFolder(dirPath);
                else{
                    if(file.getName().endsWith(".html"))
                    {
                        //masukan jsoup dsni
                        String content = "";
                        try {
                            content = ContentExtraction(file);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
        else
        {
            if(dirPath.getName().endsWith(".html"))
            { }
        }
    }

}
