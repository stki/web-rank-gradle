package ir;
import java.util.ArrayList;

public class WordProcessing {
    
    public static String Normalized(String content){
        return content.toLowerCase();
    }

    // Turn String content to ArrayList of Terms(Content)
    public static ArrayList<String> Splitter(String content){
        content = Normalized(content);
        ArrayList<String> term = new ArrayList<>();
        String pattern = "\\s+";        
        for (String word : content.split(pattern)) {
            term.add(word);
        }
        return term;
    }

    public static String Merger(ArrayList<String> words){
        StringBuilder stringBuilder = new StringBuilder();
        for (String word : words) {
            stringBuilder.append(word);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public static ArrayList<String> Crawling(ArrayList<String> dirtyList, ArrayList<String> clearnerList)
    {
        ArrayList<String> crawledWords = new ArrayList<String>();
        for(String dirtyItem : dirtyList) {
            for(String cleaner : clearnerList) {
                if(IsEqual(dirtyItem, cleaner)) {
                    crawledWords.add(dirtyItem);
                    break;
                }
            }
        }
        return crawledWords;
    }

    public static ArrayList<Integer> TermFrequency(ArrayList<String> targets, ArrayList<String> sources)
    {
        ArrayList<Integer> frequency = new ArrayList<Integer>();
        for (String target : targets) {
            int counter = 0;
            for (String source : sources) {
                if(IsEqual(target, source))
                    counter++;
            }
            frequency.add(counter);
        }
        return frequency;
    }

    public static boolean IsEqual(String target, String source){
        String first = new String(target);
        String second = new String(source);
        return first.equals(second) ? true : false;
    }

}