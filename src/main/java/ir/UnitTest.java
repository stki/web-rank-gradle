package ir;

import java.util.ArrayList;

public class UnitTest {

    String query = "how to use github";

    WordList wordList = new WordList();
    PreProcess preProcess = new PreProcess(wordList, query);
    FileManager fileManager = new FileManager(preProcess.GetLocalPath());
    ArrayList<Term> textMining = new ArrayList<Term>();

    public UnitTest()
    { }

    public UnitTest(String query)
    { 
        this.query = query;
        preProcess = new PreProcess(new WordList(), query);
    }

    public UnitTest(PreProcess preProcess, String query)
    { 
        this.preProcess = preProcess;
        this.query = query;
    }

    //#region IGNORE -TEMPORARY

    public void Run_QUERY_TEST()
    {
        String content = preProcess.ContentExtraction();
        String qualifiedContent = WordProcessing.Merger(preProcess.qualifiedTerms);
        String qualifiedQuery = WordProcessing.Merger(preProcess.queryTerms);

        // content of .html
        // System.out.println(content);
        
        // wordlist filtered .html
        System.out.println(qualifiedContent);                
        
        // word matched query
        System.out.println(qualifiedQuery);

        for (int index = 0; index < preProcess.queryTerms.size(); index++) {
            String output = preProcess.queryTerms.get(index) + " : " + preProcess.termFrequency.get(index);
            System.out.println(output); 
        }
    
    }
    //#endregion

    public void Run_FILEMANAGER_TEST()
    {
        fileManager.FileManagerListing();
    }

    public void Run_WORDLIST_TEST()
    {
        wordList.WordListGenerator();
        int i = 0;
        for (String word : wordList.WordList) 
        {
            i++;
            System.out.println(i + ". " + word);    
        }
    }
    



}