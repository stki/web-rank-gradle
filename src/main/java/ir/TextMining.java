package ir;
import java.io.File;
import java.util.ArrayList;

public class TextMining
{
    private ArrayList<ArrayList<String>> allDocTerms = new ArrayList<ArrayList<String>>();
    private ArrayList<Term> proptedQueryTerm = new ArrayList<Term>();
    private ArrayList<Double> WeigthPerIndex = new ArrayList<Double>();

    private ArrayList<ArrayList<Double>> finalWeigth = new ArrayList<ArrayList<Double>>();
    private double[] weigths = new double[10];

    private WordList wordList = new WordList();
    private PreProcess preProcessor = new PreProcess(wordList);
    private FileManager fileManager = new FileManager(preProcessor.GetLocalPath());
    
    // Get all terms in all docs
    public void TextMining_Phase1()
    {
        ArrayList<String> qualifiedDoc = new ArrayList<String>();
        for (File file : fileManager.docIndex) 
        {
            String content = preProcessor.ContentExtraction(file);
            qualifiedDoc = WordProcessing.Crawling(WordProcessing.Splitter(content), wordList.WordList);
            allDocTerms.add(qualifiedDoc);
        }
    }

    // input query
    public void TextMining_Phase2(String queryInput)
    {
        ArrayList<String> qualifiedQueryInput = WordProcessing.Crawling(WordProcessing.Splitter(queryInput), wordList.WordList);        
        for (String word : qualifiedQueryInput) 
        {
            proptedQueryTerm.add(new Term(word));
        }
    }

    // doc freq -> term ft & df
    public void TextMining_Phase3()
    {
        // System.out.println(proptedQueryTerm.size());
        for (int i = 0; i < proptedQueryTerm.size(); i++) 
        {
            for (int k = 0; k < 10; k++) 
            {
                ArrayList<String> doc = allDocTerms.get(k);
                int counter = 0;
                for (int l = 0; l < doc.size(); l++)
                {
                    if(WordProcessing.IsEqual(proptedQueryTerm.get(i).GetWord(), doc.get(l)))
                    {
                        counter++;
                    }
                    proptedQueryTerm.get(i).prop.termFrequency[k] = counter;
                    
                } 
                boolean contains = proptedQueryTerm.get(i).prop.termFrequency[k] > 0;
                proptedQueryTerm.get(i).prop.docFrequency[k] = contains;
            }
        }

    }
    
    // IDF & weigth
    public void TextMining_Phase4()
    {
        for (Term term : proptedQueryTerm) 
            term.SetInverseDocFreq(10);    
    }
        
    // get all doc, use it index to sum the IDF (proptedQ)
    // ranking
    public void TextMining_Phase5_REWORK()
    {
        for (double value : weigths) 
            value = 0;

        for(int i = 0; i < weigths.length; i++) 
        {
            for (int j = 0; j < proptedQueryTerm.size(); j++) 
            {
                weigths[i] += proptedQueryTerm.get(j).prop.termFrequency[i] * proptedQueryTerm.get(j).prop.inverseDocFreq;
            }

        }        
    }

    public void TextMining_Phase5_Debug_R()
    {
        for (int i = 0; i < weigths.length; i++) 
        {
            System.out.println(i + ". : " + weigths[i]);    
        }
    }


    // ----------------------------------------------------------------------------------------

    // DEBUG PHASE

    public void TextMining_Phase1_Debug()
    {
        ArrayList<String> temp = allDocTerms.get(2);
        for(String word : allDocTerms.get(0)) 
        {
            System.out.println(word);    
        }
    }

    public void TextMining_Phase2_Debug()
    {
        for(Term word : proptedQueryTerm) 
        {
            System.out.println(word.GetWord());    
        }
    }

    public void TextMining_Phase2p5_Debug()
    {
        Term term = proptedQueryTerm.get(0);
        for (int item : term.prop.termFrequency) 
        {
            System.out.println(item);
        }
    }

    public void TextMining_Phase3n4_Debug()
    {
        int counter = 0;
        for(Term word : proptedQueryTerm) 
        {
            System.out.print(word.GetWord());
            System.out.print(": " + word.prop.termFrequency[counter]);    
            System.out.print(" : " + word.GetDocFrequency());
            System.out.println(" : " + word.prop.inverseDocFreq);

            counter++;

        }
    }

    public void TextMining_Phase5_Debug()
    {
        int no = 1;
        for (double value : WeigthPerIndex) 
        {
            System.out.println(no++ + ". : " + value);
        }
    }

}