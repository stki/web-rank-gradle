package ir;

import java.io.File;
import java.util.ArrayList;

public class FileManager
{
    public ArrayList<File> docIndex = new ArrayList<File>();

    public FileManager(String localPath)
    {
        File[] files = new File(localPath).listFiles();

        for (File file : files) 
        {
            if(file.isFile())
                docIndex.add(file);    
        }
    }

    public void FileManagerListing()
    {
        for (File file : docIndex) 
        {
            System.out.println(file.getName());    
        }
    }
}