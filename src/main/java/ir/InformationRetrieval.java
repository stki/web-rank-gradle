package ir;

import java.util.Scanner;

public class InformationRetrieval
{    
    public static void main(String[] args) 
    {
        String querySearch = "programming bootcamp";
        // Scanner i = new Scanner(System.in);
        // System.out.println("SEARCH: ");
        // querySearch = i.next();

        TextMining textMining = new TextMining();
        textMining.TextMining_Phase1();
        textMining.TextMining_Phase2(querySearch);
        textMining.TextMining_Phase3();
        textMining.TextMining_Phase4();
        textMining.TextMining_Phase5_REWORK();

        // textMining.TextMining_Phase1_Debug();
        // textMining.TextMining_Phase2_Debug();
        // textMining.TextMining_Phase2p5_Debug();
        textMining.TextMining_Phase3n4_Debug();
        // textMining.TextMining_Phase5_Debug();
        textMining.TextMining_Phase5_Debug_R();

    }
}