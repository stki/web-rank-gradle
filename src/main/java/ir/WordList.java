package ir;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;


public class WordList {
      
    public WordList()
    {
        WordListGenerator();
    }

    public ArrayList<String> WordList = new ArrayList<String>();

    public ArrayList<String> Crawling(String content){
        return WordProcessing.Crawling(WordProcessing.Splitter(content), WordList);
    }

    public String WordListFetcher()
    {
        String path = "/web-rank/libs/wordlist.txt";
        File directory = new File(System.getProperty("user.dir"));
        directory = directory.getParentFile();
        Scanner scanner;
        
        try {
            scanner = new Scanner(new File(directory.getAbsolutePath() + path));
            String entireFileText = scanner.useDelimiter("\\A").next();
            scanner.close();
            return entireFileText;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void WordListGenerator()
    {
        String pattern = "\\W+";
        String source = WordListFetcher();

        for (String word : source.split(pattern)) {
            WordList.add(word);
        }
    }

}